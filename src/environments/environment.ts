// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig :{
    apiKey: "AIzaSyDhsSsEDeQ-MnnjMmpmfpUF1XYngDlZLCs",
    authDomain: "test2020a-1acc4.firebaseapp.com",
    databaseURL: "https://test2020a-1acc4.firebaseio.com",
    projectId: "test2020a-1acc4",
    storageBucket: "test2020a-1acc4.appspot.com",
    messagingSenderId: "1086872107464",
    appId: "1:1086872107464:web:adb93b7f7f8a18a99ae8fd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
