import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './../services/auth.service';
import { PostsService } from './../services/posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  constructor(public postsservice:PostsService,
              public authservice:AuthService,
              public db:AngularFirestore) {
              }

  posts$:Observable<any>;
  userId:string;
  likes:any;
  
  ngOnInit() {
    //this.books$ = this.bookserv.getBooks();
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    this.posts$ = this.postsservice.getPost(this.userId);
    console.log('there is '+this.posts$);

       }
    )
}

deletePost(id:string){
  this.postsservice.deletePost(id, this.userId);
}

addLikes(id:string, likes:any){
  likes ++ ; 
  this.postsservice.updateLikes(id, this.userId,likes)

}
 
  

}
