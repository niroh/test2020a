import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router) { }
  email:string;
  password:string;

  onSubmit(){
    this.authservice.login(this.email,this.password);
  }

  ngOnInit() {
  }

}
