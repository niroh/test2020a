import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth:AngularFireAuth,
              public router:Router) { 
    this.user = this.afAuth.authState;
  }

  user: Observable<User | null>

  signup(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(res => console.log('Sign-up Succeeded', this.router.navigate(['/welcome'])))
  }
  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Logged-Out', res));
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(user => {
      this.router.navigate (['/savedposts'])})
  }



}
