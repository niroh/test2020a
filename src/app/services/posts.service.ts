import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(public http:HttpClient,
              public router:Router,
              public db:AngularFirestore) { }

  private postsURL = "https://jsonplaceholder.typicode.com/posts";
  private commentsURL = "https://jsonplaceholder.typicode.com/comments";

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;

  getPosts(){
        return this.http.get<Post[]>(this.postsURL)
      }

  getComments(){
        return this.http.get<Comment[]>(this.commentsURL)
      }
  
  savePost(title:string, body:string, userId:string){
        const post = {title:title, body:body, likes:0}
       //this.db.collection('posts').add(post);
       this.userCollection.doc(userId).collection('posts').add(post);
       this.router.navigate(['/savedposts'])
     }
     getPost(userId):Observable<any[]>{
      this.postCollection = this.db.collection(`users/${userId}/posts`);
          console.log('Books collection created');
          return this.postCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
              const data = a.payload.doc.data();
              data.id = a.payload.doc.id;
              return { ...data };
            }))
          ); 
    }

    deletePost(id:string, userId:string){
      this.db.doc(`users/${userId}/posts/${id}`).delete();
    }

    updateLikes(id:string, userId:string, likes:any){
      this.db.doc(`users/${userId}/posts/${id}`).update({
        likes:likes
      })
  
    }

    
  
    
    

}
