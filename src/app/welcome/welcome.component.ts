import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authservice:AuthService) { }

  userdata$: Observable<any>;
  email:string;

  ngOnInit() {
    this.userdata$ = this.authservice.user;
    this.userdata$.subscribe(
      data => {
        this.email = data.email;
      }
    )
  }

}
