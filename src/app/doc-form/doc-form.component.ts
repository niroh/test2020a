import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassifyService } from '../services/classify.service';

@Component({
  selector: 'app-doc-form',
  templateUrl: './doc-form.component.html',
  styleUrls: ['./doc-form.component.css']
})
export class DocFormComponent implements OnInit {

  constructor(private classifyservice:ClassifyService,
    private router:Router) { }
    text:string;


  ngOnInit() {
  }
  onSubmit(){
    this.classifyservice.doc = this.text;
    this.router.navigate(['/classified']);
  }

}
