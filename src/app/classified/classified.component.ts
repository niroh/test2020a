import { AuthService } from './../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { SavedService } from './../services/saved.service';
import { ClassifyService } from './../services/classify.service';
import { Component, OnInit } from '@angular/core';
import { ImageService } from '../services/image.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classifyservice:ClassifyService,
              public imageservice:ImageService,
              public savedservice:SavedService,
              public activatedroutes:ActivatedRoute,
              public authservice:AuthService
              ) { }

  categoryImage:string;
  userId:string;
  
  categorytypes:object[]=[{name: 'business'}, {name: 'entertainment'}, {name: 'politics'}, {name: 'sport'}, {name: 'tech'}];
    
  body:string;
  aiCat:string; 
  manCat:string;
  category:string = "Loading...";


 
ngOnInit() {
  this.authservice.user.subscribe(
    user=> {
      this.userId = user.uid;
    }
  )
    this.classifyservice.classify().subscribe(
      res => {
        this.category  = this.classifyservice.categories[res];
        this.categoryImage = this.imageservice.images[res];
        this.body = this.classifyservice.doc
      }
    )
  }
  onSubmit(){
    console.log(this.body)
    console.log(this.category)
    console.log(this.manCat)
    console.log(this.userId)
    this.savedservice.saveClass(this.body, this.category, this.manCat, this.userId)
    
  }

}
