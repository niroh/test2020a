import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedclassComponent } from './savedclass.component';

describe('SavedclassComponent', () => {
  let component: SavedclassComponent;
  let fixture: ComponentFixture<SavedclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
