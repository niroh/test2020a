import { ClassifyService } from './../services/classify.service';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { SavedService } from '../services/saved.service';

@Component({
  selector: 'app-savedclass',
  templateUrl: './savedclass.component.html',
  styleUrls: ['./savedclass.component.css']
})
export class SavedclassComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router,
              public activatedroute:ActivatedRoute,
              public savedservice:SavedService) { }
  email:string;
  password:string;
  userId:string;
            
  savedClass$:Observable<any>;
            
  ngOnInit() {
  // this.savedClass$ = this.savedservice.getSavedClass();
   this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      console.log(this.userId);
  this.savedClass$ = this.savedservice.getSavedClass(this.userId);
  console.log('there is '+this.savedClass$);

     }
  )
  }

}
