import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//forms
import {FormsModule} from '@angular/forms';

//auth
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';


//angular material
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';


//api calles
import { HttpClientModule } from '@angular/common/http';

//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

//components
import { NavComponent } from './nav/nav.component';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { SavedpostsComponent } from './savedposts/savedposts.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { ClassifiedComponent } from './classified/classified.component';
import { SavedclassComponent } from './savedclass/savedclass.component';


const appRoutes: Routes = [
  //{ path: 'books', component: BooksComponent },
 // { path: 'temperatures', component: TemperaturesComponent },
 { path: 'signup', component: SignupComponent },
 { path: 'login', component: LoginComponent },
 { path: 'welcome', component: WelcomeComponent },
 { path: 'posts', component: PostsComponent },
 { path: 'savedposts', component: SavedpostsComponent },
 { path: 'docform', component: DocFormComponent },
 { path: 'classified', component: ClassifiedComponent },
 { path: 'savedclass', component: SavedclassComponent },




  { path: "",
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    PostsComponent,
    SavedpostsComponent,
    DocFormComponent,
    ClassifiedComponent,
    SavedclassComponent,
    
  ],
  imports: [

    AngularFireModule.initializeApp(environment.firebaseConfig),

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),

    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    MatFormFieldModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    MatTableModule

  ],
  providers: [
    AngularFireAuth,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
